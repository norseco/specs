Pod::Spec.new do |s|
  s.name          = "MVVM"
  s.version       = "0.0.1"
  s.summary       = "MVVM Framework"
  s.homepage      = "https://norse.co"
  s.license       = "MIT"
  s.author        = { "Leif" => "leif@norse.co" }
  s.source        = { :git => "git@bitbucket.org:norseco/mvvm.git" }
  s.platform      = :ios, '8.0'
  s.requires_arc  = true

  s.source_files  = 'MVVM/Command.swift', 'MVVM/Property.swift', 'MVVM/Query.swift', 'MVVM/MVVMBindingAdapters.swift'
  s.dependency      'RxSwift', '4.1.1'
  s.dependency      'RxCocoa', '4.1.1'
end
